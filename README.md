ecmc_server_setup Cookbook
==========================

This cookbook:
* Installs UFW
* Installs NTPd
* Installs ipmitool
* Configures NTP to use UoE DHCP setting for servers
* Creates 'cruk_admins' for CRUK Admins databag
* Creates a default firewall rule of open port 22

virtual_host
------------
* Installs libvirt, etc

Kubernetes
----------
* Installs kubernetes from Google
* Installs Google keys
* Installs etcd
* Setup interfaces

License and Authors
-------------------
Authors: Paul D Mitchell <paul.d.mitchell@ed.ac.uk>

Version: 0.9.4
