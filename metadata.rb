name             'ecmc_server_setup'
maintainer       'ECMC'
maintainer_email 'paul.mitchell@igmm.ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures ECMC servers'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.9.4'

depends 'apt'
depends 'firewall', '~> 2.4'
depends 'users'
depends 'network_interfaces_v2'
depends 'chef-vault'
depends 'docker'
depends 'hostsfile'
depends 'etcd'

supports 'ubuntu', '>= 16.04'
