# ~FC005
require 'spec_helper'

describe 'ecmc_server_setup' do
  let(:chef_run) { ChefSpec::SoloRunner.converge(described_recipe)}

  context "setup" do
    it 'includes the apt recipe' do
      expect(chef_run).to include_recipe('apt::default')
    end
  end

  context "docker" do
    it 'creates the docker service' do
      expect(chef_run).to create_docker_service('default')
    end

    it 'starts the docker service' do
      expect(chef_run).to start_docker_service('default')
    end
  end

  context "NginX setup" do
    it 'gets the image' do
      expect(chef_run).to pull_docker_image('nginx')
    end

    it 'runs the service' do
      expect(chef_run).to run_docker_container('proxy_nginx')
    end
  end
end
