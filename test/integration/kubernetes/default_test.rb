# # encoding: utf-8

# Inspec test for recipe ecmc_admins::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe user('test_user') do
  it { should exist }
end

describe group('cruk-adm') do
  it { should exist }
end

describe package('ntp') do
  it { should be_installed }
end

describe file('/etc/ntp.conf') do
  it { should exist }
end

describe file('/usr/local/share/ca-certificates/university_of_edinburgh_ca_2.crt') do
  it { should exist }
end

describe file('/etc/ssl/certs/c0a7f8f0.0') do
  it { should exist }
end

describe file('/etc/network/interfaces.d/eth1') do
  it { should exist }
end

describe package('ipmitool') do
  it { should be_installed }
end

describe package('joe') do
  it { should be_installed }
end

describe service('docker') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe package('kubelet') do
  it { should be_installed }
end

describe package('kubeadm') do
  it { should be_installed }
end

describe package('kubernetes-cni') do
  it { should be_installed }
end

describe command('apt-key list') do
  its('stdout') { should include('2048R/BA07F4FB')}
end

describe file('/proc/sys/net/ipv4/ip_forward') do
  its('content') { should eq("1\n") }
end

describe file('/etc/systemd/system/docker.service.d/10-docker-opts.conf') do
  its('content') { should match(/--iptables=false/) }
  its('content') { should match(/--ip-masq=false/) }
end

describe command('etcdctl --version') do
  its('stdout') { should match(/API version: 2/)}
  its('stdout') { should match(/etcdctl version: 3.2.17/)}
end

describe command('hostname -i') do
  its('stdout') { should include('192.168.77.1')}
end

describe command('ip route') do
  its('stdout') { should match(/10.96.0.0\/16 dev eth1.*src 192.168.77.1/)}
end
