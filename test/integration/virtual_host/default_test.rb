# # encoding: utf-8

# Inspec test for recipe ecmc_admins::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe user('test_user') do
  it { should exist }
end

describe group('cruk-adm') do
  it { should exist }
end

describe package('ntp') do
  it { should be_installed }
end

describe file('/etc/ntp.conf') do
  it { should exist }
end

describe file('/usr/local/share/ca-certificates/university_of_edinburgh_ca_2.crt') do
  it { should exist }
end

describe file('/etc/ssl/certs/c0a7f8f0.0') do
  it { should exist }
end

describe file('/etc/network/interfaces.d/eth1') do
  it { should exist }
end

describe package('ipmitool') do
  it { should be_installed }
end

describe package('joe') do
  it { should be_installed }
end

describe package('qemu-kvm') do
  it { should be_installed }
end

describe package('libvirt-bin') do
  it { should be_installed }
end

describe package('bridge-utils') do
  it { should be_installed }
end

describe package('cpu-checker') do
  it { should be_installed }
end
