include_recipe "ecmc_server_setup::default"

package 'qemu-kvm'
package 'libvirt-bin'
package 'virtinst'
package 'bridge-utils'
package 'cpu-checker'
