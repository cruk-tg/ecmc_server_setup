include_recipe 'ecmc_server_setup::default'

remote_file '/root/google.apt.key.gpg' do
  source 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
end

execute 'apt-key add /root/google.apt.key.gpg'

apt_repository 'kubernetes' do
  uri 'http://apt.kubernetes.io/'
  distribution 'kubernetes-xenial'
  components ['main']
end

package 'kubelet'
package 'kubeadm'
package 'kubernetes-cni'
package "linux-headers-#{node['kernel']['release']}"

firewall_rule 'kubernetes' do
  source "#{node['private_network']['address']}/#{node['private_network']['netmask']}"
  port 6443
  protocol :tcp
  command :allow
end

sysctl 'forwarding' do
  key 'net.ipv4.ip_forward'
  value '1'
  action :apply
end

package 'docker.io'

directory '/etc/systemd/system/docker.service.d' do
  action :create
end

file '/etc/systemd/system/docker.service.d/10-docker-opts.conf' do
  content 'Environment="DOCKER_OPTS=--iptables=false --ip-masq=false"'
  mode '0755'
  owner 'root'
  group 'root'
end

service 'docker' do
  action :restart
end

kubes = data_bag_item 'comms', 'etcd'
self_service = kubes['peers'].select { |s| s['address'] == node['private_network']['address'] }
init_cluster = kubes['peers'].collect { |n| "#{n['name']}=http://#{n['address']}:2380" }

etcd_installation 'default' do
  version '3.2.17'
  checksum '0a75e794502e2e76417b19da2807a9915fa58dcbf0985e397741d570f4f305cd'
  action :create
end

template '/etc/systemd/system/etcd.service' do
  source 'etcd.service.erb'
  variables({
    address: node['private_network']['address'],
    name: self_service.first['name'],
    init_cluster: init_cluster
  })
end

service 'etcd' do
  action :enable
end

network_interface node['private_network']['interface'] do
  bootproto 'static'
  address node['private_network']['address']
  netmask node['private_network']['netmask']
  post_up "ip route replace to 10.96.0.0/16 dev #{node['private_network']['interface']} src #{node['private_network']['address']}"
end

hostsfile_entry node['private_network']['address'] do
  hostname node['hostname']
  action :create
end
