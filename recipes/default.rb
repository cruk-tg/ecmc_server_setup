#
# Cookbook Name:: ecmc_server_setup
# Recipe:: default
#
# Copyright 2018, The University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#

include_recipe "apt::default"
include_recipe "chef-vault"

package 'ntp'
package 'ipmitool'
package 'joe'

template '/etc/ntp.conf' do
  source 'ntp.conf'
end

users_manage 'cruk-adm' do
  group_id 3001
  action [:create]
  data_bag 'users'
end

firewall 'default' do
  action :install
end

firewall_rule 'ssh' do
  port 22
  protocol :tcp
  command :allow
end

firewall_rule 'snmp' do
  port 161
  protocol :udp
  source '192.168.99.0/24'
end

execute 'update-ca-certificates' do
  command 'update-ca-certificates'
  action :nothing
end

cookbook_file '/usr/local/share/ca-certificates/university_of_edinburgh_ca_2.crt' do
  source 'uoe_ca.pem'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  notifies :run, 'execute[update-ca-certificates]', :immediately
end

if node['public_network'] && node['public_network']['interface']
  network_interface node['public_network']['interface']
end


if node['private_network'] && node['private_network']['interface']
  network_interface node['private_network']['interface'] do
    bootproto 'static'
    address node['private_network']['address']
    netmask node['private_network']['netmask']
  end
end

firewall_rule 'private' do
  raw "allow in on #{node['private_network']['interface']}"
  command :allow
end
