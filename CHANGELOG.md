ecmc_server_setup CHANGELOG
===========================

This file is used to list changes made in each version of the ecmc_server_setup cookbook.

0.1.0
-----
- [P Mitchell] - Initial release of ecmc_server_setup

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
